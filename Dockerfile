# Compile stage file file
FROM quay.io/projectquay/golang:1.20 as builder
WORKDIR /go/src/app
COPY . .
ARG TARGETARCH
ARG TARGETOS
ARG CGO_ENABLED
RUN make build TARGETARCH=$TARGETARCH TARGETOS=$TARGETOS CGO_ENABLED=$CGO_ENABLED 

# Final stage
FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/src/app/telebot /usr/local/bin/telebot
ENTRYPOINT ["/usr/local/bin/telebot"]

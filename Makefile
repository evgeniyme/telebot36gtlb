.DEFAULT_GOAL := help
SHELL := /bin/bash

APP := telebot36
VERSION := $(shell git describe --tags --abbrev=0 2>/dev/null || echo '0.0.0')-$(shell git rev-parse --short HEAD)
REGISTRY := evgenme
TARGETOS := linux#linux darwin windows OS for building
TARGETARCH := amd64
CGO_ENABLED := 0

.PHONY: help linux macOS windows arm build image push clean

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  linux            to build the Linux binary"
	@echo "  macOS            to build the macOS binary"
	@echo "  windows          to build the Windows binary"
	@echo "  arm              to build the Arm binary"
	@echo "  image            to build Docker image"
	@echo "  push             to push Docker image to repository"
	@echo "  clean            to remove the Docker image"

format:
	gofmt -s -w ./

lint:
	golint

test:
	go test -v

get:
	go get

linux:
	${MAKE} build TARGETOS=linux TARGETARCH=amd64

macOS:
	${MAKE} build TARGETOS=darwin TARGETARCH=amd64

windows:
	${MAKE} build TARGETOS=windows TARGETARCH=amd64 CGO_ENABLED=1

arm:
	${MAKE} build TARGETOS=linux TARGETARCH=arm64

build:
	CGO_ENABLED=${CGO_ENABLED} GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -v -o telebot36 -ldflags "-X="github.com/Evgeniyme/telebot36/cmd.appVersion=${VERSION}

image:
	docker build . -t ${REGISTRY}/${APP}:${VERSION}-${TARGETOS}-${TARGETARCH} --build-arg CGO_ENABLED=${CGO_ENABLED} --build-arg TARGETARCH=${TARGETARCH} --build-arg TARGETOS=${TARGETOS}

push:
	docker push ${REGISTRY}/${APP}:${VERSION}-${TARGETOS}-${TARGETARCH}

clean:
	docker rmi ${REGISTRY}/${APP}:${VERSION}-${TARGETOS}-${TARGETARCH} || true
	@echo off rm -f $(APP)